# Exercices
## Variables

### <font color=greeb>Exercice 1</font>
Affecter à $y$ la valeur $3x-2$.

{{ IDE('/Notions de base/exo1/exo1', MAX = 5, SANS = 'max,min') }}

### <font color=greeb>Exercice 2</font>
Affecter à $y$ la valeur $2x^2 - 3x + \dfrac{1}{2}$.

{{ IDE('/Notions de base/exo2/exo2', MAX = 5, SANS = 'max,min') }}

### <font color=greeb>Exercice 3</font>
Affecter à $y$ la valeur $\dfrac{x-2}{x+1}$.

{{ IDE('/Notions de base/exo3/exo3', MAX = 5, SANS = 'max,min') }}

## Fonctions

## Instructions conditionnelles



## Boucles bornées

## Boucles non bornées

## Turtle

{{ IDE('/Notions de base/Turtle/turtle', MAX = 10, SANS = 'max,min') }}
