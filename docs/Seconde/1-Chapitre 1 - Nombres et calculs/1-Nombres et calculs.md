# Chapitre 1 : Nombres et calculs

## Triangle rectangle
Écrire une fonction `is_triangle_rectangle` prenant en paramètres les longueurs des trois côtés d'un triangle et renvoyant `True` si le triangle est rectangle et `False` sinon.

{{ IDE('/Chapitre 1/is_triangle_rectangle', MAX = 5, SANS = 'max,min') }}

## Nombre premier
Écrire une fonction `is_prime` qui prend en paramètre un nombre entier `n` et qui renvoie `True` si `n` est premier et `False` sinon.

{{ IDE('/Chapitre 1/is_prime', MAX = 10, SANS = 'max,min') }}

