def ma_suite(n):
    i = 1
    u = 1
    while i < n:
        i = i + 1
        u = u + i
    return u

def table(n):
    t = []
    for i in range(n):
        t.append(ma_suite(i))
    return t