# Chapitre 1 : Second degré

## <font color=green>Discriminant</font>
Écrire une fonction `discriminant` prenant en paramètres les coefficients a, b et c et renvoyant le discriminant.

{{ IDE('/1ere/Chapitre 1/discriminant', MAX = 5, SANS = 'max,min') }}

## <font color=green>Racines</font>
Écrire une fonction `racines` qui prend en paramètres les coefficients a, b et c et qui renvoie une liste, éventuellement vide des racines du polynôme  $ax^2 + bx + c$.

{{ IDE('/1ere/Chapitre 1/racines', MAX = 10, SANS = 'max,min') }}
